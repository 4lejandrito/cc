package co.wds.incubator.java.cc;

import static co.wds.incubator.java.cc.bank.Currency.EUR;
import static co.wds.incubator.java.cc.bank.Currency.USD;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import co.wds.incubator.java.cc.bank.Money;

public class MoneyTest {

    @Test
    public void shouldEquals5DollarsTo5Dollars()
    {
        Money fiveBucks = USD.amount(5);
        Money fiveDollars = USD.amount(5);

        assertEquals(fiveBucks, fiveDollars);
    }

    @Test
    public void shouldNotEquals5DollarsTo2Dollars()
    {
        Money fiveBucks = USD.amount(5);
        Money twoBucks = USD.amount(2);

        assertNotEquals(fiveBucks, twoBucks);
    }

    @Test
    public void shouldNotEquals5DollarsTo5Euros()
    {
        Money fiveBucks = USD.amount(5);
        Money fiveEuros = EUR.amount(5);

        assertNotEquals(fiveBucks, fiveEuros);
    }

}
