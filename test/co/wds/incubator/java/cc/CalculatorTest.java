package co.wds.incubator.java.cc;

import static co.wds.incubator.java.cc.bank.Currency.*;
import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import co.wds.incubator.java.cc.bank.Bank;
import co.wds.incubator.java.cc.bank.Money;
import co.wds.incubator.java.cc.calculator.Product;
import co.wds.incubator.java.cc.calculator.Sum;

public class CalculatorTest {

    private static final Bank bank = new Bank();

    @BeforeClass
    public static void setUp()
    {
        bank.setRate(EUR, USD, 2);
        bank.setRate(EUR, GBP, 1);
        bank.setRate(EUR, AUD, 3);
    }

    @Test
    public void shouldSumDollars()
    {
        Money fiveBucks = USD.amount(5);
        Money twoBucks = USD.amount(2);

        Money sevenBucks = new Sum(bank).calculate(fiveBucks, twoBucks);

        assertEquals(USD.amount(7), sevenBucks);
    }

    @Test
    public void shouldSumDollarsWithEuros()
    {
        Money fiveBucks = USD.amount(5);
        Money twoEuros = EUR.amount(2);

        Money nineBucks = new Sum(bank).calculate(fiveBucks, twoEuros);

        assertEquals(USD.amount(9), nineBucks);
    }

    @Test
    public void shouldMultiplicateDollarsWithEuros()
    {
        Money fiveBucks = USD.amount(5);
        Money twoEuros = EUR.amount(2);

        Money nineBucks = new Product(bank).calculate(fiveBucks, twoEuros);

        assertEquals(USD.amount(20), nineBucks);
    }
}
