package co.wds.incubator.java.cc;

import static co.wds.incubator.java.cc.bank.Currency.*;
import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import co.wds.incubator.java.cc.bank.Bank;

public class BankTest {

    private static final Bank bank = new Bank();

    @BeforeClass
    public static void setUp()
    {
        bank.setRate(EUR, USD, 2);
        bank.setRate(EUR, GBP, 1);
        bank.setRate(EUR, AUD, 3);
    }

    @Test
    public void shouldUseARateEqualsToOneIfNoRateIsSet()
    {
        assertEquals(bank.convert(USD.amount(4), GBP), GBP.amount(4));
    }

    @Test
    public void shouldBeAbleToConvertBetweenCurrencies()
    {
        assertEquals(bank.convert(USD.amount(4), EUR), EUR.amount(2));
        assertEquals(bank.convert(EUR.amount(4), USD), USD.amount(8));
    }

}
