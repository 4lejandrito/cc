package co.wds.incubator.java.cc.calculator;

import co.wds.incubator.java.cc.bank.Bank;
import co.wds.incubator.java.cc.bank.Money;

public class Sum extends Operation {

    public Sum(Bank bank)
    {
        super(bank);
    }

    @Override
    public Money calculate(Money a, Money b)
    {
        Money bAsa = bank.convert(b, a.getCurrency());
        return a.getCurrency().amount(a.getAmount() + bAsa.getAmount());
    }
}
