package co.wds.incubator.java.cc.calculator;

import co.wds.incubator.java.cc.bank.Bank;
import co.wds.incubator.java.cc.bank.Money;

public abstract class Operation {

    protected final Bank bank;

    public Operation(Bank bank)
    {
        this.bank = bank;
    }

    public abstract Money calculate(Money a, Money b);
}
