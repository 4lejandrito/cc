package co.wds.incubator.java.cc.bank;

import java.util.HashMap;
import java.util.Map;

public class Bank {

    private final Map<Pair, Double> rates = new HashMap<Pair, Double>();

    public void setRate(Currency from, Currency to, double rate)
    {
        rates.put(new Pair(from, to), rate);
        rates.put(new Pair(to, from), 1 / rate);
    }

    public Double getRate(Currency from, Currency to)
    {
        Double rate = rates.get(new Pair(from, to));
        return rate != null ? rate : 1;
    }

    public Money convert(Money from, Currency to)
    {
        return new Money(to, from.getAmount() * getRate(from.getCurrency(), to));
    }
}
