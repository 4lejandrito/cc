package co.wds.incubator.java.cc.bank;

public class Money {

    private final double   amount;
    private final Currency currency;

    public Money(Currency currency, double amount)
    {
        this.currency = currency;
        this.amount = amount;
    }

    public Money sum(Money d)
    {
        return new Money(currency, getAmount() + d.getAmount());
    }

    public double getAmount()
    {
        return amount;
    }

    public Currency getCurrency()
    {
        return currency;
    }

    @Override
    public int hashCode()
    {
        return 0;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (!(obj instanceof Money))
            return false;

        Money other = (Money) obj;

        return amount == other.amount && currency.equals(other.getCurrency());
    }

    @Override
    public String toString()
    {
        return String.format("%.2f %s", amount, currency);
    }

}
