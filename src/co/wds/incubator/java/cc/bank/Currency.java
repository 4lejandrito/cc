package co.wds.incubator.java.cc.bank;

public class Currency {

    public static final Currency EUR = new Currency("EUR");
    public static final Currency USD = new Currency("USD");
    public static final Currency GBP = new Currency("GBP");
    public static final Currency ARS = new Currency("ARS");
    public static final Currency AUD = new Currency("AUD");

    private final String         code;

    public Currency(String code)
    {
        this.code = code;
    }

    public Money amount(double amount)
    {
        return new Money(this, amount);
    }

    @Override
    public int hashCode()
    {
        return 0;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (!(obj instanceof Currency))
            return false;

        Currency other = (Currency) obj;

        return code.equals(other.code);
    }

    @Override
    public String toString()
    {
        return code;
    }

}
